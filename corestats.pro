#-------------------------------------------------
#
# Project created by QtCreator 2018-08-20T09:12:54
#
#-------------------------------------------------

QT      += core gui widgets charts dbus network

TARGET   = corestats
TEMPLATE = app

VERSION  = 2.4.0

# Library section
unix:!macx: LIBS += -lcprime
unix:!macx: LIBS += -L/usr/lib -lcsys

# Disable warnings, enable threading support and c++11
CONFIG  += thread silent build_all c++11

# Disable Debug on Release
CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

# Definetion section
DEFINES += QT_DEPRECATED_WARNINGS QT_DISABLE_DEPRECATED_BEFORE=0
DEFINES += "HAVE_POSIX_OPENPT"

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

        desktop.path = $$PREFIX/share/applications/
        desktop.files = "corestats.desktop"

        icons.path = $$PREFIX/share/coreapps/icons/
        icons.files = icons/corestats.svg

        INSTALLS += target icons desktop
}


FORMS += \
    circlebar.ui \
    history_chart.ui \
    pbattery.ui \
    pdisplay.ui \
    pdrives.ui \
    pgeneral.ui \
    presources.ui \
    corestats.ui

HEADERS += \
    circlebar.h \
    history_chart.h \
    pbattery.h \
    pdisplay.h \
    pdrives.h \
    pgeneral.h \
    presources.h \
    signal_mapper.h \
    utilities.h \
    corestats.h

SOURCES += \
    circlebar.cpp \
    history_chart.cpp \
    main.cpp \
    pbattery.cpp \
    pdisplay.cpp \
    pdrives.cpp \
    pgeneral.cpp \
    presources.cpp \
    signal_mapper.cpp \
    corestats.cpp

RESOURCES += \
    icons.qrc
    
