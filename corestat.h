#ifndef CORESTAT_H
#define CORESTAT_H

#include <QWidget>

namespace Ui {
class corestat;
}

class corestat : public QWidget
{
    Q_OBJECT

public:
    explicit corestat(QWidget *parent = nullptr);
    ~corestat();

private:
    Ui::corestat *ui;
};

#endif // CORESTAT_H
