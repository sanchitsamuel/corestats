# CoreStats
A system resource viewer from the CoreApps family.

<img src="corestats.png" width="500">

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/corestats/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5-base
* qt5-charts
* [libcprime](https://gitlab.com/cubocore/libcprime) 
* [libcsys](https://gitlab.com/cubocore/libcsys) 

### Information
Please see the Wiki page for this info.[Wiki page](https://gitlab.com/cubocore/wiki) .
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the CoreBox.Send us your feedback through email or GitLab [issues](https://gitlab.com/groups/cubocore/-/issues) .
* rahmanshaber@gmail.com
* s96mini.cube@yahoo.com
