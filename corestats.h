/*
CoreStats is system resource viewer app

CoreStats is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see {http://www.gnu.org/licenses/}. */

#ifndef CORESTATS_H
#define CORESTATS_H

#include "pbattery.h"
#include "pdisplay.h"
#include "pdrives.h"
#include "pgeneral.h"
#include "presources.h"

#include <QListWidgetItem>
#include <QWidget>
#include <QPainter>
#include <QFile>
#include <QProcess>
#include <QMetaObject>
#include <QMetaProperty>
#include <QTreeWidgetItem>
#include <QDir>
#include <QSize>

#include <csys/system_info.h>
#include <csys/battery.h>
#include <csys/upower.h>

namespace Ui {
class corestats;
}

class corestats : public QWidget
{
    Q_OBJECT

public:
    explicit corestats(QWidget *parent = nullptr);
    ~corestats();

private slots:
    void on_Bresource_clicked();
    void on_Bdrives_clicked();
    void on_Bbattery_clicked();
    void on_Bgeneral_clicked();
    void on_Bdisplay_clicked();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::corestats *ui;
    void pageClick(QPushButton *btn, int i, QString title);

};

#endif // CORESTATS_H
